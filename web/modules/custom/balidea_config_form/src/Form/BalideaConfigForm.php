<?php

namespace Drupal\balidea_config_form\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Balidea Form settings for this site.
 *
 * By: Valentina Aguirre.
 */
class BalideaConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'balidea_config_form.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['balidea_config_form.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['custom_text'] = [
      '#type' => 'text_format',
      '#description' => $this->t('Please enter an informative text'),
      '#format' => $this->config('balidea_config_form.settings')->get('custom_text')['format'],
      '#rows' => 6,
      '#default_value' => $this->config('balidea_config_form.settings')->get('custom_text')['value'],
    ];

    $form['custom_integer_number'] = [
      '#type' => 'number',
      '#description' => $this->t('Please enter an integer number'),
      '#default_value' => $this->config('balidea_config_form.settings')->get('custom_integer_number'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check integer.
    $num = $form_state->getValue('custom_integer_number');
    if ($num != filter_var($form_state->getValue('custom_integer_number'), FILTER_SANITIZE_NUMBER_INT)) {
      $form_state->setErrorByName('custom_integer_number', t('Number invalid'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->configFactory->getEditable('balidea_config_form.settings');
    // Save configurations.
    $settings
      ->set('custom_text', $form_state->getValue('custom_text'))
      ->set('custom_integer_number', $form_state->getValue('custom_integer_number'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
