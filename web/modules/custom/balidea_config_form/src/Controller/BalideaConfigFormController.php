<?php

namespace Drupal\balidea_config_form\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines a Balidea Config form controller.
 *
 * By: Valentina Aguirre.
 */
class BalideaConfigFormController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $config = $this->config('balidea_config_form.settings');
    $text = $config->get('custom_text');

    $build = [
      '#theme' => 'balidea-config-form-result',
      '#custom_text' => [
        '#type' => 'processed_text',
        '#text' => $text['value'],
        '#format' => $text['format'],
      ],
      '#custom_integer' => $config->get('custom_integer_number'),
    ];

    return $build;
  }

}
