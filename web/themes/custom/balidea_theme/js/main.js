/**
 * Adds a button to body.
 *
 * @type {Drupal~behavior}
 */
(function (Drupal, once, drupalSettings) {
    Drupal.behaviors.customButton = {
      attach(context) {
        const bodyelement = once('balidea_custom', 'body', context);
        if (bodyelement.length){
          const title = Drupal.t("Try me!");
          const button = document.createElement('button');
          button.type = 'button';
          button.innerText = title;
          button.className = "button-balidea-test";
          bodyelement[0].appendChild(document.body.appendChild(button));
          loadAlert();
        }
      }
    };

    function loadAlert() {
      let btn = document.querySelector(".button-balidea-test");
      btn.addEventListener("click", event => {
        alert(drupalSettings.site.name);
      });
    }
  }(Drupal, once, drupalSettings));