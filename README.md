# balidea-test



## Instalación del sitio

1. Proceder a realizar la instalación de drupal.
2. Instalar módulo "Balidea Configuration Form (balidea_config_form)" (Solicitará instalar módulos de multilenguaje).
3. Instalar tema "Balidea" (balidea_theme).

## Caracteristicas de la prueba realizada

1. Se usan permisos propios de Drupal para el acceso al formulario de configuración
2. Incorpora un tab para la visualización de los datos guardados en el formulario 
3. Incorpora un link a la configuración del formulario desde la barra de administración de Drupal
4. Al instalar el módulo agrega un enlace en el menú principal de navegación del usuario autenticado.
5. Al desinstalar el módulo elimina el enlace en el menú principal de navegación del usuario autenticado.
6. Se utilizó el hook page_attachments_alter para pasarle a drupalSettings el nombre del sitio y así poderlo usar en JS.
7. Se utilizó un Drupal Behavior para el manejo del JS que se requiere para la funcionalidad solicitada del botón.
8. Incorpora traducciones a español en su correspondiente archivo es.po del módulo

## PHP CodeSniffer

1. Para el módulo
```
phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml web/modules/custom/balidea_config_form -v
```
2. Para el tema
```
phpcs --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml web/themes/custom/balidea_theme -v
```